using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Escena : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EscenaJuego()
    {
        SceneManager.LoadScene("Escena 1");      
    }
    
    public void EscenaMenu()
    {
        SceneManager.LoadScene("Menu");      
    }

    public void Exit()
    {
        Application.Quit();
    }
}
