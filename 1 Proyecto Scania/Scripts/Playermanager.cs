using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playermanager : MonoBehaviour
{
    private Rigidbody rigy;
    // Start is called before the first frame update
    void Start()
    {
        rigy = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (rigy.velocity.y != 0)
        {
            rigy.velocity = Vector3.zero;
        }
    }

   
    
}
