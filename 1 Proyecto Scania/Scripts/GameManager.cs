using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameManager : MonoBehaviour
{
    [Header("Revision de la falla")] 
    [SerializeField] private GameObject canvasRevisionOk;
    [SerializeField] private GameObject canvasRevisionF;


    [Header("Seleccion EPP")] 
    [SerializeField] private GameObject canvasSeleccionOk;
    [SerializeField] private GameObject canvasSeleccionF;

    [Header("Retiro viruta")]
    [SerializeField] private GameObject canvasRetiroF;

    [SerializeField]private GameObject canvasRetiroOk;
    
    
    private Viruta[] _viruta;
    [SerializeField] private GameObject canvasfinal;



    private PuertaTorno puerta;

    private ErrorHerramientas [] _errorHerramientas;

    [SerializeField] private GameObject uiHelpers;
    [SerializeField] private GameObject objlaser;
    private LineRenderer laser;
    [SerializeField] private GameObject raycaster;

    [SerializeField] private Guantes guantes;
    [SerializeField] private GameObject canvasfallacritica;


    private OVRPhysicsRaycaster raycast;

    // Start is called before the first frame update
    void Start()
    {
        _viruta = FindObjectsOfType<Viruta>();
        puerta = FindObjectOfType<PuertaTorno>();

        _errorHerramientas = FindObjectsOfType<ErrorHerramientas>();
        laser=objlaser.GetComponent<LineRenderer>();
        laser.enabled = false;

        raycast = raycaster.GetComponent<OVRPhysicsRaycaster>();
        raycast.enabled = false;

    }

    // Update is called once per frame
    void Update()
    {
        if (puerta.ErrorApertura)
        {
            canvasRevisionOk.SetActive(false);
            canvasRevisionF.SetActive(true);

        }
        else
        {
            canvasRevisionOk.SetActive(true);
            canvasRevisionF.SetActive(false);
        }



        if (_errorHerramientas[0].seleccionErroneaEPP || _errorHerramientas[1].seleccionErroneaEPP)
        {

            canvasSeleccionF.SetActive(true);
            canvasSeleccionOk.SetActive(false);
        }
        else
        {
            canvasSeleccionOk.SetActive(true);
            canvasSeleccionF.SetActive(false);
        }


        if (_viruta[0].MalSeleccionDeEquipo || _viruta[1].MalSeleccionDeEquipo || _viruta[2].MalSeleccionDeEquipo || _viruta[3].MalSeleccionDeEquipo)
        {
            canvasRetiroF.SetActive(true);
            canvasRetiroOk.SetActive(false);
        }
        else
        {
            canvasRetiroF.SetActive(false);
            canvasRetiroOk.SetActive(true);
        }

        if (_viruta[0].Damage || _viruta[1].Damage || _viruta[2].Damage || _viruta[3].Damage)
        {
            canvasfallacritica.SetActive(true);
            raycast.enabled = true;
            laser.enabled = true;
        }
    }
        
    public void GameOver()
    {
        canvasfinal.SetActive(true);
        
        
        raycast.enabled = true;
        laser.enabled = true;
    }
    
}
