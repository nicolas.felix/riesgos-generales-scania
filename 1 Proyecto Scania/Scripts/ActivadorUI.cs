using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivadorUI : MonoBehaviour
{
    [SerializeField] private Canvas canvas;

    private void OnTriggerEnter(Collider other)
    {
        canvas.gameObject.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {
        canvas.gameObject.SetActive(false);
        Destroy(gameObject);
    }
}
