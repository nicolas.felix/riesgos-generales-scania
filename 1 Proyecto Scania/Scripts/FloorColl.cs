using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorColl : MonoBehaviour
{
    private Vector3 position1;

    private Quaternion rotation;

    [SerializeField] private bool pala;
    // Start is called before the first frame update
    void Start()
    {
        position1 = transform.position;
        rotation = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.layer == 9)
        {
            if (pala)
            {
                transform.rotation = rotation;
            }

            else
            {
                transform.position = position1;
                transform.rotation = rotation;
            }
        }
        
    }
}
