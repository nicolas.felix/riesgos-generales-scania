using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimaEscena2 : MonoBehaviour
{
    private MesaDeControlEscena2 mesaControl;
    [SerializeField] private GameObject canvasMesaDeControl;
    public bool piezaControlada;
    
    // Start is called before the first frame update
    void Start()
    {
        mesaControl = FindObjectOfType<MesaDeControlEscena2>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (mesaControl.listoParaLimar||other.gameObject.layer==11)
        {
            canvasMesaDeControl.SetActive(true);
            piezaControlada = true;
        }
       
    }
}
