using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EngranajeEscena2 : MonoBehaviour
{
    private BoxCollider action;

    private Guantes guantes;
    
    

    [SerializeField] private float velRotacion;
    // Start is called before the first frame update
    void Start()
    {
        guantes = FindObjectOfType<Guantes>();
        action = FindObjectOfType<BoxCollider>();

        
        action.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (guantes.Action == true)
        {
            action.enabled = true;

        }
        
    }



    public void Rotacion()
    {

        transform.Rotate(0,0,0);
    }
}
