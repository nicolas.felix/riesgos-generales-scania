using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarroVirutero : MonoBehaviour
{
    int contador;
    GameManager manager;
    // Start is called before the first frame update
    void Start()
    {
        manager = FindObjectOfType<GameManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(contador >= 16)
        {
            manager.GameOver();
            contador = 0;
        }
    }

    void OnTriggerEnter(Collider carro)
    {
        if(carro.gameObject.layer == 8)
        {
            contador++;
            Debug.Log("contador" + contador);
        }
    }
}
