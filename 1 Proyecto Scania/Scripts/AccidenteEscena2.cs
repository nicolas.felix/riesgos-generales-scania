using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AccidenteEscena2 : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 11)
        {
            other.GetComponent<BoxCollider>().isTrigger = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        other.GetComponent<BoxCollider>().isTrigger = false;  
    }
}
