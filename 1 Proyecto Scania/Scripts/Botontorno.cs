using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Botontorno : MonoBehaviour
{

    public bool Activar { get; set; }
    [SerializeField] private GameObject luz;

    private AudioSource audio;
    // Start is called before the first frame update
    void Start()
    {
        
        audio= GetComponent<AudioSource>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.layer == 3)
        {
            audio.Stop();
            luz.gameObject.SetActive(false);
            Activar = true;
        }
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
