 using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEPPEscena2 : MonoBehaviour
{
    private Guantes guantes;

  [SerializeField]  private GameObject canvasUi;
    // Start is called before the first frame update
    void Start()
    {
        guantes = FindObjectOfType<Guantes>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay(Collider other)
    {
       
        if (other.gameObject.layer == 3 && guantes.Action==false)
        {
            canvasUi.gameObject.SetActive(true);
            
        }
        else
        {
            canvasUi.gameObject.SetActive(false);
        }
    }
}
