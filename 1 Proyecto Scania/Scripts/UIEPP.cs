using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEPP : MonoBehaviour
{
    private Botontorno boton;

    [SerializeField] private GameObject UiEpp;
    // Start is called before the first frame update
    void Start()
    {
        boton = FindObjectOfType<Botontorno>();
    }

    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        if (boton.Activar == true && other.gameObject.layer == 3)
        {
            UiEpp.gameObject.SetActive(true);
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (boton.Activar)
        {
            UiEpp.gameObject.SetActive(false);
            Destroy(gameObject);

        }


    }
}
