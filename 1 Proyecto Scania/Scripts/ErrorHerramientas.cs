using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErrorHerramientas : MonoBehaviour
{

    OVRGrabbable action;
    public bool seleccionErroneaEPP { get; set; }

    [SerializeField] private GameObject incorrecto;

     //para interface final
    // Start is called before the first frame update
    void Start()
    {
        action = GetComponent<OVRGrabbable>();
        seleccionErroneaEPP = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (action.isGrabbed)
        {
            StartCoroutine(ShowError());
            seleccionErroneaEPP = true;
        }


    }


    public IEnumerator ShowError()
    {

        do
        {
            Debug.Log("Agarrando");
            incorrecto.SetActive(true);

            yield return new WaitForEndOfFrame();
        } while (action.isGrabbed);
        Debug.Log("Solto");
        incorrecto.SetActive(false);
        yield return new WaitForEndOfFrame();
        StopCoroutine(ShowError());

    }
}


