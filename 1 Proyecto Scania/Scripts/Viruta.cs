using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Viruta : MonoBehaviour
{
     OVRGrabbable action;

    public bool Damage { get; set; }
    [SerializeField] private GameObject incorrecto;

    public bool MalSeleccionDeEquipo { get; set; } //para interface final

    private Guantes guantes;
    // Start is called before the first frame update
    void Start()
    {
        action = GetComponent<OVRGrabbable>();
        guantes = FindObjectOfType<Guantes>();
    }

    // Update is called once per frame
    void Update()
    {
        if(action.isGrabbed)
        {

            if (guantes.Action == false)
            {
                Damage = true;
            }

            if (guantes.Action == true)
            {
                MalSeleccionDeEquipo = true;
                StartCoroutine(ShowError());
            }

        }
        
      
    }


    public IEnumerator ShowError()
    {

        do
        {
            
            Debug.Log("Agarando");
            incorrecto.SetActive(true);
            
            yield return new WaitForEndOfFrame();
        } while (action.isGrabbed);
        Debug.Log("Solto");
        incorrecto.SetActive(false);
        yield return new WaitForEndOfFrame();
        StopCoroutine(ShowError());
        
        
    }
    
    
}
