using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Guantes : MonoBehaviour
{
    OVRGrabbable action;
    [SerializeField] Material materialguantes;
    [SerializeField] GameObject mano1;
    [SerializeField] GameObject mano2;



    Renderer colormano1;

    private Renderer colormano2;
    public bool Action { get; set; }

    public bool equip;



    // Start is called before the first frame update
    void Start()
    {
        action = GetComponent<OVRGrabbable>();
        colormano1 = mano1.GetComponent<Renderer>();
        colormano2 = mano2.GetComponent<Renderer>();
        Action = false;

    }


    private void Update()
    {
        if (action.isGrabbed && action)
        {
            equip = true;
        }

        if (equip && !action.isGrabbed)
        {
            colormano1.sharedMaterial = materialguantes;
            colormano2.sharedMaterial = materialguantes;
            equip = false;
            
            Action = true;
            
            Destroy(gameObject);
            

        }
    }

}
