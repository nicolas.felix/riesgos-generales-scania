using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotonMesaControl : MonoBehaviour
{
    public float velRot;
    private EngranajeEscena2 engranaje;
    // Start is called before the first frame update
    void Start()
    {
        engranaje = FindObjectOfType<EngranajeEscena2>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.layer == 3)
        { 
            engranaje.transform.Rotate(0,-velRot,0);
        }
    }
}
