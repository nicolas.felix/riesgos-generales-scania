using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuertaTorno : MonoBehaviour
{
    private Vector3 pos;

    private Botontorno boton;

    
    public bool ErrorApertura { get; set; }

    [SerializeField] private GameObject cartelAdvertencia;
    // Start is called before the first frame update
    void Start()
    {
       
        pos = transform.position;
        boton = FindObjectOfType<Botontorno>();
    }

    // Update is called once per frame


    private void Update()
    {
       


      

    }

    private void OnTriggerEnter(Collider other)
    {
        if (!boton.Activar && other.gameObject.layer==3)
        {
            transform.position = pos;
            cartelAdvertencia.SetActive(true);
            ErrorApertura = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
            cartelAdvertencia.SetActive(false);
        
    }
}

